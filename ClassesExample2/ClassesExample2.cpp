#include "stdafx.h"
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

using namespace std;
using std::string;


class Players;
vector<Players> readPlayersFile(string fileName);
void writePlayersFile(vector<Players> players, string fileName);
void printPlayers(vector<Players> players);
void search(vector<Players>, int num);

int validateInput();
bool checkIfInteger(int input);
bool checkIntegerRange(int input, int minRange, int maxRange);

void displayPlayerNames(vector<Players> players);
void editPlayer(vector<Players> &players, int position);


class Players
{
private:
	string m_playerName, m_position, m_nationality, m_team;
	int m_playerNumber;

public:
	void construct(string playerName, string position, string nationality, string team, int playerNumber);

	string getPlayerName() const;
	void setPlayerName(string playerName);

	string getPosition() const;
	void setPosition(string position);

	string getNationality() const;
	void setNationality(string nationality);

	string getTeam() const;
	void setTeam(string team);

	int getPlayerNumber() const;
	void setPlayerNumber(int playerNumber);

	void displayPlayerDetails();
	void displayPlayerNames();

};

//Getters and Setters
void Players::construct(string playerName, string position, string nationality, string team, int playerNumber)
{
	m_playerName = playerName;
	m_position = position;
	m_nationality = nationality;
	m_team = team;
	m_playerNumber = playerNumber;
}

string Players::getPlayerName() const
{
	return m_playerName;
}
void Players::setPlayerName(string playerName)
{
	m_playerName = playerName;
}

string Players::getPosition() const
{
	return m_position;
}
void Players::setPosition(string position)
{
	m_position = position;
}

string Players::getNationality() const
{
	return m_nationality;
}
void Players::setNationality(string nationality)
{
	m_nationality = nationality;
}

string Players::getTeam() const
{
	return m_team;
}
void Players::setTeam(string team)
{
	m_team = team;
}

int Players::getPlayerNumber() const
{
	return m_playerNumber;
}
void Players::setPlayerNumber(int playerNumber)
{
	m_playerNumber = playerNumber;
}

void Players::displayPlayerDetails()
{
	cout << "Player Name: " << m_playerName <<
		"\nPosition: " << m_position <<
		"\nNationality: " << m_nationality <<
		"\nTeam: " << m_team <<
		"\nPlayer Number: " << m_playerNumber << 
		"\n" << endl;
}

void Players::displayPlayerNames()
{
	cout << "> Player Name: " << m_playerName << endl;
}

bool nameComparator(const Players s1, const Players s2) //compares player names, if name is the same, compares by number
{
	if (s1.getPlayerName() != s2.getPlayerName()) return s1.getPlayerName() < s2.getPlayerName();
	return s1.getPlayerNumber() < s2.getPlayerNumber();
}

bool teamComparator(const Players s1, const Players s2) //compares player Team, if team is the same, compares by name
{
	if (s1.getTeam() != s2.getTeam()) return s1.getTeam() < s2.getTeam();
	return s1.getPlayerName() < s2.getPlayerName();
}

//Main
int main(void)
{

	string fileName = "playerData.txt"; //file for read/write

	vector<Players> players = readPlayersFile(fileName);
	Players player; //used for constructing a player before adding it to the vector
	
	if (players.empty()) //Validation, if file doesnt exist, readPlayersFile returns an empty Vector
	{
		cout << "File does not exist" << endl;
		return(-1);
	}
	
	int switchOption = 0;
	while (switchOption != 6)
	{
		cout << endl;
		cout << "\n1. Add\n2. Edit\n3. Delete\n4. Search\n5. Sort\n6. Exit and Save\n\nEnter a number between 1 - 6" << endl;
		//switchOption = validateInput();
		cin >> switchOption;
		cout << endl;
		cin.clear();

		switch (switchOption)
		{
			case 1: //add
			{
				string name, position, nationality, team;
				int number;

				cout << "Player Name: "; 
				cin >> name;
				cout << "\nPlayer Position: ";
				cin >> position;
				cout << "\nPlayer Nationality: ";
				cin >> nationality;
				cout << "\nPlayer Team: ";
				cin >> team;
				cout << "\nPlayer Number: ";
				cin >> number;


				player.construct(name, position, nationality, team, number);
				players.push_back(player);

				cout << "\nPlayer added sucessfully" << endl;
				cin.clear();
			}
			break;
			case 2: //edit
			{
				int num;

				displayPlayerNames(players); //display the names of the players found in the vector

				cout << "\nEnter the number of the player you wish to edit: ";
				cin >> num; //validation here

				cout << "\nEditing " << players[num - 1].getPlayerName() << endl;
				editPlayer(players, num - 1);

				cout << "\nPlayer mended successfully!";
			}
			break;
			case 3: //delete
			{
				int num;

				displayPlayerNames(players); //display the names of the players found in the vector

				cout << "\nEnter the number of the player you wish to delete: ";
				cin >> num; //validation here
				
				// erase the player the user entered
				players.erase(players.begin() + (num - 1)); 

				cout << "\nPlayer deleted successfully!";
			}
			break;
			case 4: //search
			{
				
				cout << "1. Player Name\n2. Position\n3. Nationality\n4. Team\n5. Number\n\nPlease enter a number between 1 - 5" << endl;
				int num;
				cin >> num;
				if (num < 1 || num > 5) //Validation
				{
					while (num < 1 || num > 5)
					{
						cout << "Invalid number. Try again" << endl;
						cin >> num;
					}
				}
				search(players, num);
				cin.clear();
			}
			break;
			case 5: //sort
			{
				vector<Players> sortVector = players; //keeping the integrity of the original file

				printPlayers(players);

				//cout << "SORTED PLAYERS BY NAME" << endl;
				//sort(sortVector.begin(), sortVector.end(), nameComparator); //sorts by name and number
				//cout << "SORTED PLAYERS BY TEAM" << endl;
				//sort(sortVector.begin(), sortVector.end(), teamComparator); //sorts by team and name
				//printPlayers(sortVector);
			}
			break;
			case 6: //save and exit
			{
				//writePlayersFile(players, fileName); //uncomment to write
			}
			break;
		}
	}
	//printPlayers(players);
	//writePlayersFile(players, fileName);
	
	

	return 0;

}

void printPlayers(vector<Players> players)
{
	for (vector<Players>::iterator i = players.begin(); i != players.end(); i++)
	{
		Players position = *i;
		position.displayPlayerDetails();
	}
}

vector<Players> readPlayersFile(string fileName)
{
	vector<Players> players;
	Players player;

	ifstream readFile(fileName); //read


	if (!readFile) //file doesnt exist
	{
		return(players); //return empty vector, if vector is empty. file doesnt exist
	}

	do //do while for retrieving names from file
	{
		string oneWord, position, nationality, team;
		string name = "";
		int number;

		do //this should be a function
		{
			readFile >> oneWord;

			name += oneWord;
			if (oneWord[oneWord.size() - 1] != ';')
			{
				name += ' ';
			}
		} while (oneWord[oneWord.size() - 1] != ';');

		name = name.substr(0, name.size() - 1); //removes the delimitter at the end of each string - make a function 

		readFile >> position;

		readFile >> nationality;

		readFile >> team;

		readFile >> number;

		player.construct(name, position, nationality, team, number); //construct the player

		players.push_back(player); //push player into Players vector

	} while (!readFile.eof());

	readFile.close(); //close file

	return players;
}

void writePlayersFile(vector<Players> players, string fileName)
{
	cout << "Saving Players.....";

	ofstream writePlayerData(fileName); //write players to file
	if (writePlayerData)
	{
		for (vector<Players>::iterator i = players.begin(); i != players.end(); i++)
		{
			Players position = *i;
			writePlayerData << 
				position.getPlayerName() << "; " <<
				position.getPosition() << " " <<
				position.getNationality() << " " <<
				position.getTeam() << " " <<
				position.getPlayerNumber();


			if (i != players.end() - 1) //Stops going to a new line after the last player is written
			{
				writePlayerData << "\n";
			}
		}
	}
	else
	{
		cout << "Error Opening file" << endl;
	}

	writePlayerData.close();
	cout << " Successful!" << endl;
}


void search(vector<Players> players, int num)
{

	vector<double>::size_type size = players.size();

	switch (num)
	{
		case 1: //search by name
		{
			string name;
			cin.ignore(); //ignores all characters entered into cin. Using this to get a line from the input
			cout << "\nEnter the name of the player you wish to see: ";
			getline(std::cin, name);
			cout << endl;

			auto it = find_if(players.begin(), players.end(), [&name](const Players& obj) {return obj.getPlayerName() == name; });

			if (it != players.end())
			{
				// found element. it is an iterator to the first matching element.
				// if you really need the index, you can also get it:
				auto index = distance(players.begin(), it);
				players[index].displayPlayerDetails();
			}
			cin.clear();
		} 
		break;
		case 2: //search by position
		{
			string position;
			cin.ignore(); //ignores all characters entered into cin. Using this to get a line from the input
			cout << "\nEnter the position of the player you wish to see: ";
			getline(std::cin, position);
			cout << endl;

			int count = 0;
			for (int i = 0; i < size; i++)
			{
				if (players[i].getPosition().compare(position) == 0)
				{
					players[i].displayPlayerDetails();
					count++;
				}
			}
			if (count == 0)
			{
				cout << position << " does not exist" << endl;
			}
		}
		break;
		case 3: //search by nationality
		{
			string nationality;
			cin.ignore(); //ignores all characters entered into cin. Using this to get a line from the input
			cout << "\nEnter the nationality of the player you wish to see: " << endl;
			getline(std::cin, nationality);
			cout << endl;

			int count = 0;
			for (int i = 0; i < size; i++)
			{
				if (players[i].getNationality().compare(nationality) == 0)
				{
					players[i].displayPlayerDetails();
					count++;
				}
			}
			if (count == 0)
			{
				cout << nationality << " does not exist" << endl;
			}
		}
		break;
		case 4: //search by team
		{
			string team;
			cin.ignore(); //ignores all characters entered into cin. Using this to get a line from the input
			cout << "Enter the name of the player you wish to see: " << endl;
			getline(std::cin, team);
			cout << endl;

			int count = 0;
			for (int i = 0; i < size; i++)
			{
				if (players[i].getTeam().compare(team) == 0)
				{
					players[i].displayPlayerDetails();
					count++;
				}
			}
			if (count == 0)
			{
				cout << team << " does not exist" << endl;
			}
		}
		break;
		case 5: //search by number
		{
			int number;
			cin.ignore(); //ignores all characters entered into cin. Using this to get a line from the input
			cout << "Enter the name of the player you wish to see: " << endl;
			cin >> number;
			cout << endl;

			int count = 0;
			for (int i = 0; i < size; i++)
			{
				if (players[i].getPlayerNumber() == number)
				{
					players[i].displayPlayerDetails();
					count++;
				}
			}
			if (count == 0)
			{
				cout << number << " does not exist" << endl;
			}
		}
		break;
	}
}

int validateInput() //need to check if its an integar and in the range of 1 - 6
{
	bool aChar = false;
	bool outOfRange = false;

	int userInput;

	do
	{
		cin >> userInput;

		//Re - initilize bools if input is incorrect more than once
		aChar = false;
		outOfRange = false;

		aChar = checkIfInteger(userInput);
		if (aChar == false)
		{
			outOfRange = false;
		}
		else
		{
			outOfRange = checkIntegerRange(userInput, 1, 6); //number of options in the menu
		}
		
		cout << aChar << " and " << outOfRange << endl;

		cin.clear();
		cin.ignore();
	} 
	while (aChar == true && outOfRange == true);
	

	return userInput;
}

bool checkIfInteger(int input)
{
	if (!cin) //checks if not an integer
	{
		cout << "Invalid, try again: ";
		return false;
	}
	else
	{
		cout << "ITS TRUE -- not a char" << endl;
		return true;
	}
}

bool checkIntegerRange(int input, int minRange, int maxRange)
{
	if (input < minRange || input > maxRange) //check in range
	{
		cout << "Out of range: ";
		return false;
	}
	else
	{
		cout << "ITS TRUE -- in the range" << endl;
		return true;
	}
}

void displayPlayerNames(vector<Players> players)
{
	vector<double>::size_type size = players.size();
	for (int i = 0; i < size; i++)
	{
		cout << "\t" << i + 1;
		players[i].displayPlayerNames();
	}
}

void editPlayer(vector<Players> &players, int pos)
{
	string name, position, nationality, team;
	int number, userNum;
	cout << pos << endl;
	cout << "What would you like to edit?\n\n1. Name\n2. Position\n3. Nationality\n4. Team\n5. Number" << endl;
	cin >> userNum;

	switch (userNum)
	{
	case 1:
	{
		cout << "New Name: ";
		cin >> name;
		players[pos].setPlayerName(name);
		cout << "Name updated successfully" << endl;
	}
	break;
	case 2:
	{
		cout << "New Position: ";
		cin >> position;
		players[pos].setPosition(position);
		cout << "Position updated successfully" << endl;
	}
	break;
	case 3:
	{
		cout << "New Nationality: ";
		cin >> nationality;
		players[pos].setNationality(nationality);
		cout << "Nationality updated successfully" << endl;
	}
	break;
	case 4:
	{
		cout << "New Team: ";
		cin >> team;
		players[pos].setTeam(team);
		cout << "Team updated successfully" << endl;
	}
	break;
	case 5:
	{
		cout << "New Number: ";
		cin >> number;
		players[pos].setPlayerNumber(number);
		cout << "Number updated successfully" << endl;
	}
	break;
	}
}
